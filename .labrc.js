const modules = require('./lib/modules.js');

// Set up paths to tests based on module name
let pathsArray = [];
for (let mod of modules) {
	pathsArray.push(`lib/${mod}/${mod}.tests.js`);
}

module.exports = {
	paths: pathsArray,
	globals: '@@any-promise/REGISTRATION,__core-js_shared__,CSS,regeneratorRuntime,core'	,
	verbose: true,
	assert: '@hapi/code',
	timeout: 10000,
	leaks: true,
};