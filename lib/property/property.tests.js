const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const lab = exports.lab = Lab.script();

lab.experiment('Property', () => {
  let server;
  // let sequelize;
  let propertyId1;
  let propertyId2;
  let accessToken;
  let accountId;
  let adminAccessToken;

  //Duplicate Address Test Variables
  let propertyCountBefore;

  lab.before(async() => {
    const index = await require('../../index.js');
    server = await index.server;
    // sequelize = await index.sequelize;
    await index.sequelize;

    // Login
    const tokenResponse = await getAccessToken();
    accessToken = tokenResponse.access_token;

    // Post an Account
    const accountResponse = await server.inject({
      method: 'GET',
      url: '/accounts',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });

    let accountPayload = JSON.parse(accountResponse.payload);
    accountId = accountPayload.id;

    // Login
    const adminTokenResponse = await getAdminAccessToken();
    adminAccessToken = adminTokenResponse.access_token;

  });


  lab.test('Pretest: GET /properties Empty', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/properties',
    });
    const payload = JSON.parse(response.payload)
    expect(payload).to.equal([]);
  });

  lab.test('GET /accounts/properties Empty', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/accounts/properties',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });
    const payload = JSON.parse(response.payload)
    expect(payload.length).to.equal(0);
  });


  lab.test('POST /properties, no-auth', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '229 North Craig St, Pgh',
      }
    });

    const payload = JSON.parse(response.payload);
    // save the propertyId for later so we can clean up
    propertyId1 = payload.id;
    expect(payload.name).to.equal('229 North Craig Street, Pittsburgh, PA 15213');
    expect(payload.machineName).to.equal('229-north-craig-street-pittsburgh-pa-15213');
    expect(payload.Parcels[0].parcelId).to.equal('0027D00094000000');
    expect(payload.Parcels[0].PropertyId).to.equal(propertyId1);

  });

  lab.test('GET /properties Contains One', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/properties',
    });
    const payload = JSON.parse(response.payload)
    expect(payload.length).to.equal(1);
  });

  // TODO: POST property with bad AuthorId, get 400:"message": "Must have an Account to create a Property with any field other than address"

  lab.test('GET /accounts/properties Still Empty', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/accounts/properties',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });
    const payload = JSON.parse(response.payload)
    expect(payload.length).to.equal(0);
  });

  lab.test('POST /properties, auth', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'POST',
      url: '/properties',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
      payload: {
        'address': '230 North Craig St, Pgh',
        // it's ok to send numeric fields as number or string
        // bathrooms are allowed 1-9 in .25 increments
        'bathroomsMin': '1',
        'bathroomsMax': 8.75,
        // bedrooms can be 0-10
        'bedroomsMin': 0,
        'bedroomsMax': '10',
        'AuthorId': accountId,
      }
    });

    const payload = JSON.parse(response.payload);
    // These are sequelize fields that we don't need to worry about
    delete payload.createdAt;
    delete payload.updatedAt;
    // console.log('property payload', payload);

    expect(payload.name).to.equal('230 North Craig Street, Pittsburgh, PA 15213');
    expect(payload.machineName).to.equal('230-north-craig-street-pittsburgh-pa-15213');
    // fields stored as decimal are returned as strings
    expect(payload.bathroomsMin).to.equal('1');
    expect(payload.bathroomsMax).to.equal('8.75');
    expect(payload.bedroomsMin).to.equal(0);
    expect(payload.bedroomsMax).to.equal(10);
    expect(payload.AuthorId).to.equal(accountId);

    // save the propertyId for later so we can clean up
    propertyId2 = payload.id;
  });

  lab.test('GET /properties Contains Two Ordered by New', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/properties',
    });
    const payload = JSON.parse(response.payload)
    expect(payload.length).to.equal(2);
    expect(payload[0].id).to.equal(propertyId2)
    expect(payload[1].id).to.equal(propertyId1)
  });

  lab.test('GET /properties/machine-name/{machine-name} Contains Includes', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: `/properties/machine-name/230-north-craig-street-pittsburgh-pa-15213`,
    });
    const payload = JSON.parse(response.payload)
    expect(payload.id).to.equal(propertyId2);
    expect(payload).to.include(['PostalAddresses', 'GeoCoordinate']);
  });

  lab.test('GET /accounts/properties Contains One Property', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/accounts/properties',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });
    const payload = JSON.parse(response.payload)
    expect(payload.length).to.equal(1);
  });

  lab.test('PATCH /properties/{propertyId}, admin', {timeout: 5000}, async () => {
    const response = await server.inject({
      method: 'PATCH',
      url: `/properties/${propertyId2}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
      payload: {
        'address': '147 North Craig St, Pgh',
      }
    });
    const payload = JSON.parse(response.payload);
    expect(payload.name).to.equal('147 North Craig Street, Pittsburgh, PA 15213');
    expect(payload.PostalAddresses[0].streetAddress).to.equal('147 North Craig Street');
    expect(payload.Parcels[0].parcelId).to.equal('0027D00190000000');
    expect(payload.Parcels[0].PropertyId).to.equal(propertyId2);
  });

  lab.test('Test Review Count', async () => {
    // Post a landlord to review
    const propertyResponse = await server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '462 S Atlantic Ave',
      },
    });
    const property = JSON.parse(propertyResponse.payload);

    // Add First Review
    const postFirstReviewPayload = {
      subject: 'Review 1',
      body: 'This is a review to test review count.',
      rating: 5,
    };
    await server.inject({
      method: 'POST',
      url: `/properties/${property.id}/reviews`,
      payload: postFirstReviewPayload,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    // Check for 1 review 
    const response1 = await server.inject({
      method: 'GET',
      url: `/properties/${property.id}`,
    });
    const payload1 = JSON.parse(response1.payload);
    expect(payload1.metadata).to.include({reviewCount: 1});

    // Add Second Review
    const postSecondReviewPayload = {
      subject: 'Review 2',
      body: 'This is another review to test review count.',
      rating: 5,
    };
    await server.inject({
      method: 'POST',
      url: `/properties/${property.id}/reviews`,
      payload: postSecondReviewPayload,
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });

    // Check for 2 reviews
    const response2 = await server.inject({
      method: 'GET',
      url: `/properties/${property.id}`,
    });
    const payload2 = JSON.parse(response2.payload);
    expect(payload2.metadata).to.include({reviewCount: 2});

    await server.inject({
      method: 'DELETE',
      url: '/properties/' + property.id,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });
  });

  lab.test('PATCH /properties/{id}/merge, merge properties', async () => {
    const response = await server.inject({
      method: 'PATCH',
      url: `/properties/${propertyId1}/merge`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
      payload: {
        'mergeInto': propertyId2,
      }
    });

    const payload = JSON.parse(response.payload);
    expect(payload.PostalAddresses.length).to.equal(2);
  });
  
  // TODO: POST property by admin with user AuthorId

  // DELETE from user account, fail
  lab.test('DELETEs /properties/{propertyId}, user fail', async () => {
    // wait for the response and the request to finish
    const response1 = await server.inject({
      method: 'DELETE',
      url: '/properties/' + propertyId1,
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });

    const payload1 = JSON.parse(response1.payload);
    // Users shouldn't be able to delete: 403 forbidden
    expect(response1.statusCode).to.equal(403);
  });

  // DELETE from Admin account, success
  lab.test('DELETEs /properties/{propertyId}, admin', async () => {
    // Add A Review to test if it gets deleted
    const reviewPayload = {
      subject: 'Am I deleted?',
      body: 'This is a review to test review deletion upon property deletion.',
      rating: 5,
    };
    await server.inject({
      method: 'POST',
      url: `/properties/${propertyId1}/reviews`,
      payload: reviewPayload,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    // wait for the response and the request to finish
    const response1 = await server.inject({
      method: 'DELETE',
      url: '/properties/' + propertyId1,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    const payload1 = JSON.parse(response1.payload);
    // one row should have been affected
    expect(payload1).to.equal(1);
    expect(response1.statusCode).to.equal(202);

    // check if property's postal address also deleted
    const addressResponse = await server.inject({
      method: 'GET',
      url: '/addresses',
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    const addressesPayload = JSON.parse(addressResponse.payload);

    for(let postalAddress of addressesPayload) {
      expect(postalAddress).to.not.contain({PropertyId: propertyId1});
    }
    // check if property's geoCoordinates also deleted
    const geosResponse = await server.inject({
      method: 'GET',
      url: '/geo-coordinates',
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    const geosPayload = JSON.parse(geosResponse.payload);
    for(let geoCoordinate of geosPayload) {
      expect(geoCoordinate).to.not.contain({PropertyId: propertyId1});
    }

    // check if property's reviews also deleted
    const reviewsResponse = await server.inject({
      method: 'GET',
      url: '/reviews',
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    const reviewsPayload = JSON.parse(reviewsResponse.payload);
    for(let review of reviewsPayload) {
      expect(review).to.not.contain({aliasableId: propertyId1});
    }

    // wait for the response and the request to finish
    const response2 = await server.inject({
      method: 'DELETE',
      url: '/properties/' + propertyId2,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    const payload2 = JSON.parse(response2.payload);
    // one row should have been affected
    expect(payload2).to.equal(1);
    expect(response2.statusCode).to.equal(202);
  });


  lab.test('Posttest: GET /properties Empty', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: '/properties'
    });
    const payload = JSON.parse(response.payload)
    expect(payload).to.equal([]);
  });


  lab.test('POST /properties, duplicate address, setup part 1', async () => {
    const propertiesResponse = await server.inject({
      method: 'GET',
      url: '/properties',
    });

    const propertiesPayload = JSON.parse(propertiesResponse.payload);
    propertyCountBefore = propertiesPayload.length;
  });

  lab.test('POST /properties, duplicate address, setup part 2', async () => {
    const initialPromise = server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '5022 Rosetta St, Pgh',
      }
    });
    
    const promiseDuplicate1 = server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '5022 Rosetta St, Pgh',
      }
    });

    const promiseDuplicate2 = server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '5022 Rosetta St, Pgh',
      }
    });
    
    const promiseDuplicate3 = server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '5022 Rosetta St, Pgh',
      }
    });

    const allPromises = [initialPromise, promiseDuplicate1, promiseDuplicate2, promiseDuplicate3];

    await Promise.all(allPromises);
  });

  lab.test('POST /properties, duplicate address, fail', async () => {
    const propertiesResponse = await server.inject({
      method: 'GET',
      url: '/properties',
    });

    const propertiesPayload = JSON.parse(propertiesResponse.payload);
    const propertyCountAfter = propertiesPayload.length;

    expect(propertyCountAfter - propertyCountBefore).to.equal(1);
  });
});

function getAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.TEST_USER,
        password: process.env.TEST_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}

function getAdminAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.SUPER_ADMIN_USER,
        password: process.env.SUPER_ADMIN_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}
