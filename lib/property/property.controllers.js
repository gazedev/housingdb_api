module.exports = (models) => {
  const Boom = require('@hapi/boom');
  const Sequelize = require('sequelize');
  const Op = Sequelize.Op;
  const parcelLib = require('../parcel/parcel.controllers')(models).lib;
  const postalAddressLib = require('../postal-address/postal-address.controllers')(models).lib;
  const geoCoordinatesLib = require('../geo-coordinates/geo-coordinates.controllers')(models).lib;
  const locationLib = require('../location/location.controllers')(models).lib;
  const propertyModels = require('./property.models');
  const accountLib = require('../account/account.controllers')(models).lib;
  const helpersLib = require('../helpers')(models).lib;

  return {
    adminGenerateMachineNames: async function(request, h) {
      let machineNamelessProperties;
      try {
        // Match machine names that are null or contain parenthesis or brackets
        let regexp = '[()\\[\\]]';
        // Searches for null machine names, or ones that match regexp
        machineNamelessProperties = await getAllPropertiesByMachineName({
          [Op.or]: {
            [Op.regexp]: regexp,
            [Op.is]: null,
          },
        });
        for (let property of machineNamelessProperties) {
          let machineName = await createMachineName(property.PostalAddresses[0].address);
          property.update({machineName: machineName});
        }
      } catch (e) {
        throw Boom.badImplementation('Error during adminGenerateMachineNames. ' + e);
      }
      return machineNamelessProperties;
    },
    getProperties: async function(request, h) {
      let returnProperties;
      try {
        returnProperties = await getProperties(request.query);
      } catch (e) {
        throw Boom.badImplementation('Error during getProperties(). ' + e);
      }
      return h
        .response(returnProperties);
    },
    getProperty: async function(request, h) {
      let returnProperty;
      try {
        returnProperty = await getProperty(request.params.id);
      } catch (e) {
        throw Boom.badImplementation('Error during getProperty(...). ', e);
      }
      return h
        .response(returnProperty);
    },
    getPropertyByMachineName: async function(request, h) {
      let returnProperty;
      try {
        returnProperty = await getPropertyByMachineName(request.params.machineName);
      } catch (e) {
        throw Boom.badImplementation('Error during getPropertyByMachineName(...).', e);
      }
      return h
        .response(returnProperty);
    },
    postProperty: async function(request, h) {
      /*
        # Require Account for submitting fields other than address
      */
      let account;
      let payload = request.payload;
      let payloadKeys = Object.keys(payload);
      if (payloadKeys.length !== 1 || payloadKeys[0] !== 'address') {
        // not only address set, user must be authd
        let noAuth;
        try {
          if (request.auth.credentials) {
            account = await accountLib.getAccount(request.auth.credentials);
          } else {
            noAuth = true;
          }
        } catch (e) {
          throw Boom.badImplementation('Error during getAccount(request.auth.credentials). ' + e);
        }

        if (account === null || noAuth) {
          // User doesn't have an account, operation not allowed
          throw Boom.badRequest('Must have an Account to create a Property with any field other than address');
        }
      }

      /* Restrict AuthorId to same user, or SuperAdmin */
      if (payload.AuthorId) {
        // If AuthorId is set then we have already passed auth and account lookup
        // This is because if AuthorId is set then not only quickInfo is set
        if (
          payload.AuthorId !== account.id  &&
          request.auth.credentials.subjectId !== process.env.SUPER_ADMIN
        ) {
          throw Boom.badRequest('Property.AuthorId must match your Account ID or you must be a Super Admin');
        }
      }

      let addressObject;
      let firstGeocode;
      try {
        // Process the address
        firstGeocode = await postalAddressLib.getFirstGeocode(payload.address, 'property')
        addressObject = await postalAddressLib.getProcessedAddressFromGeocode(firstGeocode, 'property');
      } catch (e) {
        throw Boom.badImplementation('Error processing address!' + e)
      }

      // Ensure Address doesn't already exist
      const existingAddresses = await postalAddressLib.getAddressesByHash(addressObject.hash);
      if (existingAddresses.length > 0) {
        let error = Boom.badData('Address already exists');
        error.output.headers["Content-Location"] = existingAddresses[0].PropertyId;
        throw error;
      }

      // bathrooms validation
      let hasBathrooms = false;
      let bathroomsMin = Number(request.payload.bathroomsMin);
      let bathroomsMax = Number(request.payload.bathroomsMax);
      if (bathroomsMin || bathroomsMax) {
        hasBathrooms = true;
      }

      // if one value is set, they both should be set
      if (hasBathrooms && !bothDefined(bathroomsMin, bathroomsMax)) {
        throw Boom.badData('if either bathroomsMin or bathroomsMax is set, both must be set');
      }

      // min should be less than max
      if (hasBathrooms && (bathroomsMin > bathroomsMax)) {
        throw Boom.badData('bathroomsMin should be less than bathroomsMax');
      }

      // bathrooms can be in .25 increments
      if (hasBathrooms &&
        (
          !bathroomsValidIncrement(bathroomsMin) ||
          !bathroomsValidIncrement(bathroomsMax)
        )
      ) {
        throw Boom.badData('bathroomsMin and bathroomsMax must be in .25 increments');
      }

      // bedrooms validation
      let hasBedrooms = false;
      let bedroomsMin = request.payload.bedroomsMin;
      let bedroomsMax = request.payload.bedroomsMax;
      if (bedroomsMin || bedroomsMax) {
        hasBedrooms = true;
      }

      // if one value is set, they both should be set
      if (hasBedrooms && !bothDefined(bedroomsMin, bedroomsMax)) {
        throw Boom.badData('if either bedroomsMin or bedroomsMax is set, both must be set');
      }

      // min should be less than max
      if (hasBedrooms && (bedroomsMin > bedroomsMax)) {
        throw Boom.badData('bedroomsMin should be less than bedroomsMax');
      }

      // start building property object
      let propertyObject = {};
      if (request.payload.name && request.payload.name !== request.payload.address) {
        propertyObject.name = request.payload.name;
      } else {
        propertyObject.name = addressObject.address;
      }

      if (request.payload.AuthorId) {
        propertyObject.AuthorId = request.payload.AuthorId;
      }

      if (hasBathrooms) {
        propertyObject.bathroomsMin = bathroomsMin;
        propertyObject.bathroomsMax = bathroomsMax;
      }

      if (hasBedrooms) {
        propertyObject.bedroomsMin = bedroomsMin;
        propertyObject.bedroomsMax = bedroomsMax;
      }

      if (request.payload.website) {
        propertyObject.website = request.payload.website;
      }

      if (request.payload.contact) {
        propertyObject.contact = request.payload.contact;
      }

      if (request.payload.body) {
        propertyObject.body = request.payload.body;
      }

      let aggregateProperty = await createPropertyAndAssociations(propertyObject, addressObject, firstGeocode)

      return h
        .response(aggregateProperty);

    },
    patchProperty: async function(request, h) {
      let propertyId = request.params.id;
      let propertyInstance = await getProperty(propertyId);
      let propertyObject = {};

      // Make sure property exists
      if (propertyInstance === null) {
        throw Boom.badRequest('Property does not exist');
      }

      // Throw if no account found
      let account = request.pre.account;

      // If User not Admin, must own content, cannot submit 'AuthorId' or 'address' fields
      if (request.auth.credentials.subjectId !== process.env.SUPER_ADMIN) {
        if (propertyInstance.AuthorId !== account.id) {
          throw Boom.forbidden('Property.AuthorId must match your Account ID or you must be a Super Admin');
        }

        if (request.payload.hasOwnProperty('AuthorId')) {
          throw Boom.badRequest('Only an Admin can change the AccountId');
        }

        if (request.payload.hasOwnProperty('address')) {
          throw Boom.badRequest('Only an Admin can change the address');
        }
      }

      // If address change, update it and related fields
      let locationObject;
      let addressObject;
      if (request.payload.hasOwnProperty('address')) {
        let firstGeocode;
        try {
          firstGeocode = await postalAddressLib.getFirstGeocode(request.payload.address);
          addressObject = await postalAddressLib.getProcessedAddressFromGeocode(firstGeocode);
        } catch (e) {
          throw Boom.badImplementation('Error processing address! ' + e)
        }
      
        // Get location
        locationObject = geoCoordinatesLib.extractLocation(firstGeocode);
        // Make sure address doesn't already exist,
        // Returns additional context if address matches current property
        // 'address' should not be sent on PATCH if it is not changing
        const existingAddresses = await postalAddressLib.getAddressesByHash(addressObject.hash);
        if (existingAddresses.length > 0) {
          if (
            existingAddresses.length === 1 &&
            existingAddresses[0].PropertyId === propertyId
          ) {
            throw Boom.badData('Address already exists, and matches this property');
          }
          throw Boom.badData('Address already exists');
        }
        // If name not set on payload, and old name == old address
        // make new name = new address
        let previousName = propertyInstance.get('name');
        let previousAddressObject = propertyInstance.get('PostalAddresses', {plain:true})[0];
        let previousStandardizedAddress = postalAddressLib.standardizeAddress(previousAddressObject);
        if (
          !request.payload.hasOwnProperty('name') &&
          previousName == previousStandardizedAddress
        ) {
          propertyObject.name = addressObject.address;
        }
        // If address change, update machine name
        propertyObject.machineName = await createMachineName(addressObject.address);
        // Update Location: wait to persist until the end
        // Update Postal Address: wait to persist until the end

        // returns:
        // - standardizedAddress
        // - addressObject
        // - locationObject
      }

      // AuthorId change, shouldn't need anything else
      if (request.payload.hasOwnProperty('AuthorId')) {
        propertyObject.AuthorId = request.payload.AuthorId;
      }

      // Name
      if (request.payload.hasOwnProperty('name')) {
        propertyObject.name = request.payload.name;
      }

      // Validate bedrooms
      let hasBedrooms = false;
      let bedroomsMin;
      // set hasBedrooms if one set
      // min: prefer new, then old, otherwise undefined
      if (request.payload.hasOwnProperty('bedroomsMin')) {
        hasBedrooms = true;
        bedroomsMin = request.payload.bedroomsMin;
      } else if (propertyInstance.get('bedroomsMin') !== null) {
        hasBedrooms = true;
        bedroomsMin = propertyInstance.get('bedroomsMin');
      }
      let bedroomsMax;
      // high: if new, then old, then null
      if (request.payload.hasOwnProperty('bedroomsMax')) {
        hasBedrooms = true;
        bedroomsMax = request.payload.bedroomsMax;
      } else if (propertyInstance.get('bedroomsMax') !== null) {
        hasBedrooms = true;
        bedroomsMax = propertyInstance.get('bedroomsMax');
      }
      // if hasBedrooms and not bothDefined, error
      if (hasBedrooms && !bothDefined(bedroomsMin, bedroomsMax) && !bothNull(bedroomsMin, bedroomsMax)) {
        throw Boom.badData('if either bedroomsMin or bedroomsMax is set, both must be set');
      }
      // if low bedroom greater than high bedroom, error
      if (hasBedrooms && (bedroomsMin > bedroomsMax)) {
        throw Boom.badData('bedroomsMin should be less than bedroomsMax');
      }

      // Validate bathrooms
      let hasBathrooms = false;
      let bathroomsMin;
      if (request.payload.hasOwnProperty('bathroomsMin')) {
        hasBathrooms = true;
        if (request.payload.bathroomsMin === null) {
          bathroomsMin = null;
        } else {
          bathroomsMin = Number(request.payload.bathroomsMin);
        }
      } else if (propertyInstance.get('bathroomsMin') !== null) {
        hasBathrooms = true;
        bathroomsMin = Number(propertyInstance.get('bathroomsMin'));
      }
      let bathroomsMax;
      if (request.payload.hasOwnProperty('bathroomsMax')) {
        hasBathrooms = true;
        if (request.payload.bathroomsMax === null) {
          bathroomsMax = null;
        } else {
          bathroomsMax = Number(request.payload.bathroomsMax);
        }
      } else if (propertyInstance.get('bathroomsMax') !== null) {
        hasBathrooms = true;
        bathroomsMax = Number(propertyInstance.get('bathroomsMax'));
      }

      if (hasBathrooms && !bothDefined(bathroomsMin, bathroomsMax)  && !bothNull(bathroomsMin, bathroomsMax)) {
        throw Boom.badData('if either bathroomsMin or bathroomsMax is set, both must be set');
      }

      if (hasBathrooms && (bathroomsMin > bathroomsMax)) {
        throw Boom.badData('bathroomsMin should be less than bathroomsMax');
      }

      if (hasBathrooms &&
        (
          !bathroomsValidIncrement(bathroomsMin) ||
          !bathroomsValidIncrement(bathroomsMax)
        )
      ) {
        throw Boom.badData('bathroomsMin and bathroomsMax must be in .25 increments');
      }

      if (hasBedrooms) {
        propertyObject.bedroomsMin = bedroomsMin;
        propertyObject.bedroomsMax = bedroomsMax;
      }
      if (hasBathrooms) {
        propertyObject.bathroomsMin = bathroomsMin;
        propertyObject.bathroomsMax = bathroomsMax;
      }
      // Website doesn't require extra validation
      if (request.payload.hasOwnProperty('website')) {
        propertyObject.website = request.payload.website;
      }
      // Contact doesn't require extra validation
      if (request.payload.hasOwnProperty('contact')) {
        propertyObject.contact = request.payload.contact;
      }
      // Body doesn't require extra validation
      if (request.payload.hasOwnProperty('body')) {
        propertyObject.body = request.payload.body;
      }
      // Create property using payload
      let updatedProperty = await propertyInstance.update(propertyObject);
      if (request.payload.hasOwnProperty('address')) {
        await updatedProperty.PostalAddresses[0].update(addressObject)
        await updatedProperty.GeoCoordinate.update(locationObject);
        return updatedProperty.reload();
      }

      const returnPropertyWithParcels = await updateParcelList(updatedProperty);
      
      return returnPropertyWithParcels;
    },
    deleteProperty: async function(request, h) {
      const propertyId = request.params.id;
      
      let response;
      const t = await models.Property.sequelize.transaction();
      try {
        await postalAddressLib.deleteAddressByPropertyId(propertyId, t);

        await geoCoordinatesLib.deleteGeoCoordinatesByPropertyId(propertyId, t);

        //  Note: we can't call the reviewLib without causing circular dependency
        // await reviewLib.deleteReviewByReviewableId(propertyId, t);
        await models.Review.destroy({
          where: {
            reviewableId: propertyId,
          },
          transaction: t,
        });
        
        response = await models.Property.destroy({
          where: {
            id: propertyId,
          },
          transaction: t,
        });

        await t.commit();
      } catch (e) {
        console.log('Rollback!', e);
        await t.rollback();
        // throw Boom.badImplementation('Error during models.Property.destroy(...).', e);
      }
      return h
        .response(response)
        .code(202);
    },
    getAccountProperties: async function(request, h) {
      let account = request.pre.account;

      return await getAccountProperties(account.id);
    },
    updateAllPropertyParcels: async function(request, h){
      let allProperties;
      try {
        allProperties = await getProperties();
      } catch (e) {
        throw Boom.badImplementation('Error during getProperties(). ' + e);
      }

      for(let property of allProperties){
        try {
          await updateParcelList(property);
        } catch (e) {
          console.log('Error Updating a Parcel', e)
        }

      }

      try {
        allProperties = await getProperties();
      } catch (e) {
        throw Boom.badImplementation('Error during getProperties(). ' + e);
      }

      return allProperties;      
    },
    mergeDuplicateProperties: async function(request, h) {
      // Get Source property to merge into Destination property
      let sourceId = request.params.id;
      let destinationId = request.payload.mergeInto;
      
      // Get and make sure first property exists
      let sourceInstance = await getProperty(sourceId);
      if (sourceInstance === null) {
        throw Boom.badRequest('Merge Source Property does not exist');
      }

      // Get and make sure second property exists
      let destinationInstance = await getProperty(destinationId);
      if (destinationInstance === null) {
        throw Boom.badRequest('Merge Destination Property does not exist');
      }

      let sourceProperty = sourceInstance.get({plain: true});

      // NOTE: We can't call get plain on the destination instance or it will no longer be an instance
      // .toJSON won't mutate the original instance, so we can safely use it here.
      let destinationProperty = destinationInstance.toJSON();

      // Set up body to contain alternate info. Store in array to later join in the body. 
      const destinationBodyArray = [ destinationProperty.body, sourceProperty.body, 'Alternate Info:'];

      destinationBodyArray.push(`Name: ${sourceProperty.name}`);

      for(let field of [ 'body', 'contact', 'website' ]){
        if(sourceProperty[field]){
          if(sourceProperty[field] !== destinationProperty[field] && destinationProperty[field]){
            destinationBodyArray.push(
              `${field.charAt(0).toUpperCase() + field.slice(1)}: ${sourceProperty[field]}`
            );
          }
          if(!destinationProperty[field]){
            destinationProperty[field] = sourceProperty[field];
          }
        }
      }

      destinationProperty.body = destinationBodyArray.join('\n');

      if(sourceProperty.AuthorId && !destinationProperty.AuthorId){
        destinationProperty.AuthorId = sourceProperty.AuthorId;
      }

      if(sourceProperty.LandlordId && !destinationProperty.LandlordId){
        destinationProperty.LandlordId = sourceProperty.LandlordId;
      }

      // Attach any associations on source to destination
      let updatedDestinationProperty;
      const t = await models.Property.sequelize.transaction();
      try {
        // Move PostalAddreses from Source to Destination
        await models.PostalAddress.update({
          PropertyId: destinationProperty.id,
        },
        {
          where: {
            PropertyId: sourceProperty.id 
          },
          transaction: t
        });

        // Move reviews from source to destination landlord
        await models.Review.update({
          reviewableId: destinationInstance.id
        }, 
        {
          where: {
            reviewableType: 'property',
            reviewableId: sourceInstance.id
          }, 
          transaction: t
        });

        // Move external reviews from source to destination landlord
        await models.ExternalReview.update({
          reviewableId: destinationInstance.id
        }, 
        {
          where: {
            reviewableType: 'property',
            reviewableId: sourceInstance.id
          }, 
          transaction: t
        });
        
        // Update destination with new info from source
        updatedDestinationProperty = await destinationInstance.update(destinationProperty, {transaction: t});

        await t.commit();
      } catch {
        await t.rollback();
      }

      await helpersLib.updateReviewCountAverage(sourceInstance);
      await helpersLib.updateReviewCountAverage(destinationInstance);
      updatedDestinationProperty = await updateParcelList(updatedDestinationProperty)

      return updatedDestinationProperty;
    },
    lib: {
      getProperty: getProperty,
      createProperty: createProperty,
      createPropertyAndAssociations: createPropertyAndAssociations, 
      getPropertyOptions: getPropertyOptions,
    }

  };




  /*
   * Puts a property object in the DB and returns an instance
   */
  function createProperty(propertyObject) {
    return models.Property.create(propertyObject)
  }

  function getProperties(queryParams = {}) {
    return models.Property.findAll(getPropertyOptions(queryParams));
  }

  function getProperty(id) {
    return models.Property.findByPk(id, getPropertyOptions())
  }

  function getPropertyByMachineName(machineName) {
    return models.Property.findOne(getPropertyOptions({
      machineName:machineName
    }));
  }

  function getAllPropertiesByMachineName(machineName) {
    return models.Property.findAll({
      where: {machineName: machineName},
      include: [
        {model: models.PostalAddress},
      ]
    });
  }

  async function createPropertyAndAssociations(propertyObject, addressObject, geocode){
    // Create geoCoordObject from geocode
    let geoCoordObject = geoCoordinatesLib.extractLocation(geocode);
    // Add machine Name
    propertyObject.machineName = await createMachineName(addressObject.address);
    
    // Get the LocationId
    propertyObject.LocationId = await locationLib.getLocationIdFromAddress(addressObject);
    
    // Create the Property
    let propertyInstance = await createProperty(propertyObject);

    // Set the addressObject PropertyId to the new property
    addressObject.PropertyId = propertyInstance.id;

    // Create PostalAddress with the PropertyId in addressObject
    await postalAddressLib.createAddress(addressObject);

    geoCoordObject.PropertyId = propertyInstance.id;
    // Create a location with the PropertyId in geoCoordObject
    await geoCoordinatesLib.createLocation(geoCoordObject);

    //Get the property with all associations
    const returnProperty = await getProperty(propertyInstance.id);

    // update the Parcels of this property
    const returnPropertyWithParcels = await updateParcelList(returnProperty);

    return returnPropertyWithParcels;
  }

  async function createMachineName(name){
    const space = / /gi; //replaces spaces with -
    const replace = /[/,.()\[\]]/gi; // characters to replace in a machine name
    const remove = /[!?@#$%^&*<>;:|]/gi; //removes characters within []
    const duplicate = /-+/gi; //bye bye duplicate dashes
    const trimming = /^[-]|[-]$/gi // ^ = leading characters, $ = trailing characters


    name = name.toLowerCase();
    name = name.replace(space, "-"); //replaces spaces with -
    name = name.replace(replace, "-"); //replaces characters within [] with a -
    name = name.replace(remove, ""); //removes characters within []
    name = name.replace(duplicate, "-"); //bye bye duplicate dashes
    name = name.replace(trimming, ""); // ^ = leading characters, $ = trailing characters


    let unique = false;
    let counter = 0;

    let machineName = name;


    while(!unique) {
      // TODO: efficiency could be improved with an Op.like lookup
      let propertyExists = await getPropertyByMachineName(machineName);
      if(propertyExists === null) {unique = true}
      else {
        counter ++;
        machineName = name + "-" + counter;
      }

    }

    return machineName;
  }

  function getPropertyOptions(queryParams = {}) {
    let whereParams = {};
    let propertyOptions = {order: [['createdAt', 'DESC']]};
    propertyOptions.include = [
      {model: models.Parcel},
      {model: models.PostalAddress},
      {model: models.GeoCoordinates},
    ];
    // hasOwnProperty workaround because query object overwritten, see:
    // https://github.com/hapijs/hapi/issues/3280
    if (Object.prototype.hasOwnProperty.call(queryParams, 'search')) {
      // If there's a 'search' query param, we ignore the others and search
      // specific fields for that value
      propertyOptions.where = {
        [Op.or]: [
          {name: {[Op.iLike]: '%'+ queryParams.search +'%'}},
          {"$PostalAddresses.address$": {[Op.iLike]: '%'+ queryParams.search +'%'}}
        ]
      };
      return propertyOptions;
    }
    if (Object.prototype.hasOwnProperty.call(queryParams, 'machineName')) {
      // If we're getting a property by machine name, we ignore other params and
      // just query a machineName match.
      propertyOptions.where = {
        machineName: queryParams.machineName
      };
      return propertyOptions;
    }
    // else, no 'search' or 'machineName' queryParam
    if (Object.prototype.hasOwnProperty.call(queryParams, 'name')) {
      whereParams.name = {
        [Op.iLike]: '%' + queryParams.name + '%',
      };
    }
    if (Object.prototype.hasOwnProperty.call(queryParams, 'bedrooms')) {
      whereParams.bedroomsMin = {
        [Op.lte]: queryParams.bedrooms,
      };
      whereParams.bedroomsMax = {
        [Op.gte]: queryParams.bedrooms,
      };
    }
    if (Object.prototype.hasOwnProperty.call(queryParams, 'locations')) {
      whereParams.LocationId = queryParams.locations;
    }
    if (Object.prototype.hasOwnProperty.call(queryParams, 'address')) {
      whereParams["$PostalAddresses.address$"] = {
        [Op.iLike]: '%' + queryParams.address + '%',
      };
    }
    if (Object.keys(whereParams).length !== 0) {
      propertyOptions.where = whereParams;
    }
    return propertyOptions;
  }

  function bothDefined (value1, value2) {
    let value1Defined = (value1 !== undefined && value1 !== null);
    let value2Defined = (value2 !== undefined && value2 !== null);
    let bothDefined = (value1Defined && value2Defined);
    return bothDefined;
  }

  function bothNull (value1, value2) {
    let bothNull = (value1 === null && value2 === null);
    return bothNull;
  }

  function bathroomsValidIncrement(value) {
    // multiply by 100 to take valid values out of decimal form, to work around
    // javascript modulo errors with floating point numbers. equivalent to value % .25
    let remainder = (value * 100) % 25;

    // remainder 0 means exactly divisible, or a correct multiple of .25
    if (remainder === 0) {
      return true;
    }

    return false;
  }

  function getAccountProperties(accountId) {
    let propertyOptions = {
      where: {
        AuthorId: accountId,
      },
    };
    return models.Property.findAll(propertyOptions);
  }

  async function updateParcelList(property){
    property = property.get({plain: true})

    if(!property.PostalAddresses){
      throw Boom.badData('Property must contain PostalAddresses. Use getProperty().')
    }

    const addresses = property.PostalAddresses;
    const oldParcelList = property.Parcels;
    let oldPinList = [];
    let newPinList = [];

    // Get list of only PINs (parcelIds)
    for(let parcel of oldParcelList){
      oldPinList.push(parcel.parcelId)
    }

    // Add each pin from each address associated with property
    for (let address of addresses){
      let addressParcelIds;
      try {
        addressParcelIds = await parcelLib.getParcelId(address.address);  
      } catch (e) {
        throw Boom.badImplementation('Error getting parcelIds!', e)
      }

      // Ensure addressParcelIds was returned as an array (rather than null/undefined/error/etc) so we can iterate over it
      if(!Array.isArray(addressParcelIds)){
        throw Boom.badImplementation("Error getting address's parcel ids")
      }

      // Get list of new PINs
      for( let parcel of addressParcelIds) {
        newPinList.push(parcel.pin);
      }
    }

    // Deduplicate the array then return it
    newPinList = [...new Set(newPinList)];
    
    // Compare changes in parcel lists
    // If a new pin exists with no matching old pin, add it to db
    // If old pin exists with no matching new pin, delete it from db
    let parcelsToAdd = [];
    let parcelsToRemove = [];
    
    // Store old pins to remove from DB if not included among newPins 
    for(let pin of oldPinList){
      if(!newPinList.includes(pin)){
        parcelsToRemove.push(pin);
      }
    }
    
    // Store new pins to add to DB if not included among oldPins
    for(let pin of newPinList){
      if(!oldPinList.includes(pin)){
        parcelsToAdd.push(
          { 
            parcelId: pin,
            PropertyId: property.id
          }
        );
      }
    }

    const t = await models.Parcel.sequelize.transaction();
    try {
      // Remove parcels from DB
      if(parcelsToRemove.length > 0){
        await models.Parcel.destroy({ 
          where: {
            parcelId: parcelsToRemove,
          },
          transaction: t,
        })
      }
      
      // Add parcels to DB
      if(parcelsToAdd.length > 0){
        await models.Parcel.bulkCreate(parcelsToAdd, { transaction: t });
      }   
      
      await t.commit();
    } catch (e) {
      console.log('Rolled Back Parcel update!', e)
      await t.rollback()
    }

    return getProperty(property.id)
  }
};
