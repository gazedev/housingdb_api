const Boom = require('@hapi/boom');

module.exports = (models) => {
  let accountLib = require('./account/account.controllers.js')(models).lib;
  return {
    ensureAdmin: async function(request) {
      if (
        // if SUPER_ADMIN is undefined or empty
        !process.env.SUPER_ADMIN ||
        !request.auth.credentials  || request.auth.credentials.subjectId !== process.env.SUPER_ADMIN
      ) {
        throw Boom.forbidden('Must be an Admin');
      }
      return true;
    },
    ensureAccount: async function(request) {
      let account;
      try {
        account = await accountLib.getAccount(request.auth.credentials);
      } catch (e) {
        throw Boom.badImplementation('Error during getAccount(request.auth.credentials). ' + e);
      }

      if (account === null) {
        throw Boom.badRequest('Must have an Account');
      }

      return account;
    },
    lib: {
      updateReviewCountAverage: updateReviewCountAverage,
    },
  };

  async function updateReviewCountAverage(reviewableItem){
    if (!reviewableItem) {
      throw Boom.badRequest('ReviewableItem does not exist');
    }

    let reviews;
    try {
      reviews = await models.Review.findAll({
        where: {
          reviewableId: reviewableItem.id,
        }
      });
    } catch (e) {
      throw Boom.badImplementation("Failed to get this reviewable's reviews")
    }
    
    const reviewCount = reviews.length;
    let reviewAverage;
    if(reviewCount === 0){
      reviewAverage = 0;
    }
    else {
      let ratingSum = 0;
      for (let review of reviews) {
        ratingSum += review.rating;
      };
  
      reviewAverage = ratingSum / reviewCount;
    }

    try {
      await reviewableItem.set('metadata.reviewCount', reviewCount).save();
      await reviewableItem.set('metadata.reviewAverage', reviewAverage).save();
    } catch (e) {
      throw Boom.badImplementation('Error updating the metadata.reviewCount or metadata.reviewAverage', e);
    };
  }
};
