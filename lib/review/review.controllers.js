module.exports = (models) => {
  const Boom = require('@hapi/boom');
  const reviewModels = require('./review.models');
  const accountLib = require('../account/account.controllers')(models).lib;
  const landlordLib = require('../landlord/landlord.controllers')(models).lib;
  const propertyLib = require('../property/property.controllers')(models).lib;


  return {
    getReviews: async function(request, h) {
      let account = request.pre.account;

      return await getAccountReviews(account.id);
    },
    getLandlordReviews: async function(request, h) {
      const landlordId = request.params.id;
      return await getItemReviews(landlordId, 'landlord');
    },
    getPropertyReviews: async function(request, h) {
      const propertyId = request.params.id;
      return await getItemReviews(propertyId, 'property');
    },
    getAllReviews: async function (request, h) {
      return await getAllReviews()
    },
    // postReview: async function (request, h) {
    //
    // },
    postLandlordReview: async function (request, h) {
      let reviewableType = 'landlord';
      let reviewableId = request.params.id;

      let account = request.pre.account;

      let reviewableItem;
      try {
        reviewableItem = await getReviewableItem(reviewableType, reviewableId);
      } catch (e) {
        throw Boom.badImplementation('Error during getReviewableItem(reviewableType, reviewableId). ' + e);
      }

      if (reviewableItem === null) {
        throw Boom.badRequest('ReviewableItem does not exist');
      }

      let existingReview;
      try {
        existingReview = await getOneAccountItemReview(
          account.id,
          reviewableId,
          reviewableType,
        );
      } catch (e) {
        throw Boom.badImplementation('Error during getOneAccountItemReview(...). ' + e);
      }

      if (existingReview !== null) {
        throw Boom.badRequest('This Account has already created a Review for this Item');
      }

      let reviewObject = {
        AuthorId: account.id,
        subject: request.payload.subject,
        body: request.payload.body,
        rating: request.payload.rating,
        reviewableType: reviewableType,
        reviewableId: reviewableId,
      };

      let review;
      try {
        review = await createReview(reviewObject);
      } catch (e) {
        throw Boom.badImplementation('Error during createReview(reviewObject). ' + e);
      }
      await reviewableItem.reload();
      
      await updateReviewCountAndAverage(reviewableItem);

      return review;
    },
    postPropertyReview: async function (request, h) {
      let reviewableType = 'property';
      let reviewableId = request.params.id;

      let account = request.pre.account;

      let reviewableItem;
      try {
        reviewableItem = await getReviewableItem(reviewableType, reviewableId);
      } catch (e) {
        throw Boom.badImplementation('Error during getReviewableItem(reviewableType, reviewableId). ' + e);
      }

      if (reviewableItem === null) {
        throw Boom.badRequest('ReviewableItem does not exist');
      }

      let existingReview;
      try {
        existingReview = await getOneAccountItemReview(
          account.id,
          reviewableId,
          reviewableType
        );
      } catch (e) {
        throw Boom.badImplementation('Error during getAccountItemsReviews(...). ' + e);
      }

      if (existingReview !== null) {
        throw Boom.badRequest('This Account has already created a Review for this Item');
      }

      let reviewObject = {
        AuthorId: account.id,
        subject: request.payload.subject,
        body: request.payload.body,
        rating: request.payload.rating,
        reviewableType: reviewableType,
        reviewableId: reviewableId,
      };

      let review;
      try {
        review = await createReview(reviewObject);
      } catch (e) {
        throw Boom.badImplementation('Error during createReview(reviewObject). ' + e);
      }

      await reviewableItem.reload();
      
      await updateReviewCountAndAverage(reviewableItem);

      return review;
    },
    deleteReview: async function (request, h) {      
      const reviewId = request.params.id;     
      let review;
      try {
        review = await models.Review.findOne({
          where: {
            id: reviewId,
          }
        });
      } catch (e) {
        throw Boom.badRequest('Failed to find review to be deleted');
      }
      
      let account = request.pre.account; 
      
      // If User not Admin, must own content to delete
      if (request.auth.credentials.subjectId !== process.env.SUPER_ADMIN) {
        if (review.AuthorId !== account.id) {
          throw Boom.forbidden('review.AuthorId must match your Account ID or you must be a Super Admin');
        }
      }      

      // Get the landlord or property to update reviewCount on
      const reviewableType = review.reviewableType;
      const reviewableId = review.reviewableId;
      let reviewableItem;
      
      try {
        reviewableItem = await getReviewableItem(reviewableType, reviewableId);
      } catch (e) {
        throw Boom.badImplementation('Error during getReviewableItem(reviewableType, reviewableId). ' + e);
      }

      let response;
      try {
        response = await models.Review.destroy({
          where: {
            id: reviewId,
          },
        });
      } catch (e) {
        throw Boom.badImplementation('Error during models.reviews.destroy(...);.', e);
      }
      
      await updateReviewCountAndAverage(reviewableItem);

      return h
        .response(response)
        .code(202);
    },
  };

  function getReviewOptions() {
    return {
      attributes: { exclude: ['AuthorId'] },
    }
  }

  function getItemReviews(reviewableId, reviewableType) {
    // combine our local query object with getReviewOptions object
    let reviewOptions = getReviewOptions();
    if (reviewOptions.where === undefined) {
      reviewOptions.where = {};
    }
    reviewOptions.where.reviewableType = reviewableType;
    reviewOptions.where.reviewableId = reviewableId;
    return models.Review.findAll(reviewOptions);
  }

  function getAccountReviews(accountId) {
    // combine our local query object with getReviewOptions object
    let reviewOptions = getReviewOptions();
    if (reviewOptions.where === undefined) {
      reviewOptions.where = {};
    }
    reviewOptions.where.AuthorId = accountId;
    return models.Review.findAll(reviewOptions);
  }

  function getAllReviews() {
    let reviewOptions = getReviewOptions();
   
    reviewOptions.order = [['updatedAt', 'DESC']];
    return models.Review.findAll(reviewOptions);
  }
  
  function getOneAccountItemReview(accountId, reviewableId, reviewableType) {
    // combine our local query object with getReviewOptions object
    let reviewOptions = getReviewOptions();
    if (reviewOptions.where === undefined) {
      reviewOptions.where = {};
    }
    reviewOptions.where.AuthorId = accountId;
    reviewOptions.where.reviewableType = reviewableType;
    reviewOptions.where.reviewableId = reviewableId;
    return models.Review.findOne(reviewOptions);
  }

  async function getReviewableItem(reviewableType, reviewableId) {
    let getItem;
    if (reviewableType === 'landlord') {
      getItem = landlordLib.getLandlord;
    } else if (reviewableType === 'property') {
      getItem = propertyLib.getProperty;
    } else {
      throw Boom.badRequest('Invalid reviewableType requested');
    }

    let reviewableItem;
    try {
      reviewableItem = await getItem(reviewableId);
    } catch (e) {
      throw Boom.badImplementation('Error during getItem(reviewableId). ' + e);
    }

    return reviewableItem;
  }

  function createReview(reviewObject) {
    return models.Review.create(reviewObject)
  }

  async function updateReviewCountAndAverage(reviewableItem){
    if (!reviewableItem) {
      throw Boom.badRequest('ReviewableItem does not exist');
    }

    let reviews;
    try {
      reviews = await models.Review.findAll({
        where: {
          reviewableId: reviewableItem.id,
        }
      });
    } catch (e) {
      throw Boom.badImplementation("Failed to get this reviewable's reviews")
    }
    
    const reviewCount = reviews.length;
    let reviewAverage;
    if(reviewCount === 0){
      reviewAverage = 0;
    }
    else {
      let ratingSum = 0;
      for (let review of reviews) {
        ratingSum += review.rating;
      };
  
      reviewAverage = ratingSum / reviewCount;
    }

    try {
      await reviewableItem.set('metadata.reviewCount', reviewCount).save();
      await reviewableItem.set('metadata.reviewAverage', reviewAverage).save();
    } catch (e) {
      throw Boom.badImplementation('Error updating the metadata.reviewCount or metadata.reviewAverage', e);
    };
  };

  // async function deleteReviewByReviewableId(reviewableId, transaction = undefined) {
    // if(transaction) {
    //   return await models.Review.destroy({
    //     where: {
    //       reviewableId: reviewableId,
    //     },
    //     transaction: transaction,
    //   });
    // } else {
    //   return await models.Review.destroy({
    //     where: {
    //       reviewableId: reviewableId,
    //     },
    //   });
    // }
  // }
};
