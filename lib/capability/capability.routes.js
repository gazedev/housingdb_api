module.exports = {
  routes: (models) => {
    const controllers = require('./capability.controllers.js')(models);

    return [
      {
        method: 'GET',
        path: '/capabilities',
        handler: controllers.getBaseCapabilities,
        options: {
          auth: false,
          description: 'Returns the base capabilities',
          notes: 'Returns the base level capabilities for modules',
          tags: ['api', 'Capabilities'],
        }
      },
    ];

  },
};