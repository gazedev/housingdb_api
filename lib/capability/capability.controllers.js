module.exports = (models) => {
  const evictionsLib = require('../eviction/eviction.controllers')(models).lib;
  
  return {
    getBaseCapabilities: function(req, h) {
      return {
        "/evictions:system": evictionsLib.capabilityEvictionsSystem(),
      };
    },
  }
  
}