module.exports = (models) => {
  const Boom = require('@hapi/boom');

  return {
    lib: {
      getParcelId: getParcelId,
    }
  };

  function getParcelId(address){
    const api = new URL(process.env.PARCEL_ADDRESS_API);

    let https;
    if (api.protocol == 'http:') {
      https = require('http');
    } else {
      https = require('https');
    }
    
    api.searchParams.set('address', address);

    let promise = new Promise((resolve, reject) => {
      https.get(api, res => {
        res.setEncoding("utf8");
        let body = "";
        res.on("data", data => {
          body += data;
        });
        res.on("end", () => {
          try {
            body = JSON.parse(body);
          } catch (e) {
            // log the error, but don't throw here or we will crash the api
            console.log(e);         
          }
          resolve(body);
        });
        res.on('error', error => {
          reject(error);
        });
      });

    });

    return promise;
  };
};
