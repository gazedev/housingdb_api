const Joi = require('joi');

module.exports = {
  db: (sequelize, Sequelize) => {
    const Parcel = sequelize.define('Parcel', {
      uuid: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
      },
      parcelId:  Sequelize.STRING,
    },
    {
      indexes:[
       {
         unique: false,
         fields:['parcelId']
       }
      ]
    });

    return Parcel;
  },
  address: Joi.object().keys({
    address: Joi.string().required(),  
  })

};