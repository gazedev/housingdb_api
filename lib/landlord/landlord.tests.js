const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const lab = exports.lab = Lab.script();

lab.experiment('Landlord', () => {
  let server;

  let landlord;
  let landlordLib;
  let parseInfo;
  let accessToken;
  let accountId;
  let adminAccessToken;
  let landlordId;
  let mergeSourceId;
  let mergeDestinationId;
  let mergePropertyId;
  let mergeSourceName;
  
    lab.before(async() => {
      const index = await require('../../index.js');
      server = await index.server;
      let sequelize = await index.sequelize;

      // Login
      const tokenResponse = await getAccessToken();
      accessToken = tokenResponse.access_token;

      // Post an Account
      const accountResponse = await server.inject({
        method: 'GET',
        url: '/accounts',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });

      accountId = JSON.parse(accountResponse.payload).id;

      landlord = await require('./landlord.controllers')(sequelize.models);
      landlordLib = landlord.lib;
      parseInfo = landlordLib.parseInfo;

      // Login
      const adminTokenResponse = await getAdminAccessToken();
      adminAccessToken = adminTokenResponse.access_token;
    });

    lab.test('parses landlord string', async () => {
      let text = 'Example Landlord 412.444.5555ext45 landlord@example.com';
      let searches = ['phone', 'email'];
      let remove = true;

      expect(parseInfo(text, searches, remove)).to.equal({
        email: 'landlord@example.com',
        phone: {
          original: '412.444.5555ext45',
          formatted: '(412) 444-5555 ext. 45',
          countryCode: 1,
          nationalNumber: 4124445555,
          extension: '45',
        },
        remainder: 'Example Landlord',
      });
    });

    lab.test('GET /landlords empty', async () => {
      const response = await server.inject('/landlords');
      const payload = JSON.parse(response.payload)
      expect(payload).to.equal([]);
    });

    lab.test('GET /accounts/landlords Empty', async () => {
      const response = await server.inject({
        method: 'GET',
        url: '/accounts/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });
      const payload = JSON.parse(response.payload)
      expect(payload).to.equal([]);
    });

    lab.test('POST /landlords, un-authed', async () => {
      const postPayload = {
        quickInfo: 'Some Landlord',
      };
      const response = await server.inject({
        method: 'POST',
        url: '/landlords',
        payload: postPayload,
      });
      const payload = JSON.parse(response.payload);
      expect(payload).to.include({
        name: postPayload.quickInfo
      });
      landlordId = payload.id;
    });

    lab.test('GET /landlords Contains One', async () => {
      const response = await server.inject('/landlords');
      const payload = JSON.parse(response.payload);
      expect(payload.length).to.equal(1);
      expect(payload[0]).to.include({id: landlordId})
    });

    lab.test('GET /landlords/{landlordId} fail', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/landlords/7c4f6716-9c1c-4692-a96f-f320ec91938e`,
      });
      const payload = JSON.parse(response.payload);
      expect(payload).to.include({statusCode: 404});
    });

    lab.test('GET /landlords/machine-name/{bad-name} fail', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/landlords/machine-name/bad-machine-name`,
      });
      const payload = JSON.parse(response.payload);
      expect(payload).to.include({statusCode: 404});
    });

    lab.test('GET landlords/{landlordId}', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/landlords/${landlordId}`,
      });

      const payload = JSON.parse(response.payload);
      expect(payload).to.include({
        id: landlordId,
        name: 'Some Landlord',
        machineName: 'some-landlord',
      });
    });

    lab.test('POST /landlords, Another un-authed', async () => {
      const postPayload = {
        quickInfo: 'Another Landlord',
      };
      const response = await server.inject({
        method: 'POST',
        url: '/landlords',
        payload: postPayload,
      });
      const payload = JSON.parse(response.payload);
      expect(payload).to.include({
        name: postPayload.quickInfo
      });
    });

    lab.test('GET /landlords Ordered by New', async () => {
      const response = await server.inject('/landlords');
      const payload = JSON.parse(response.payload)
      expect(payload[0].name).to.equal('Another Landlord');
      expect(payload[1].name).to.equal('Some Landlord');
    });

    lab.test('GET /accounts/landlords Still Empty', async () => {
      const response = await server.inject({
        method: 'GET',
        url: '/accounts/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });
      const payload = JSON.parse(response.payload)
      expect(payload).to.equal([]);
    });

    lab.test('POST/PATCH /landlords, authed', async () => {
      const postPayload = {
        name: 'Authd Landlord',
        AuthorId: accountId,
      };
      const postResponse = await server.inject({
        method: 'POST',
        url: '/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
        payload: postPayload,
      });
      const postParse = JSON.parse(postResponse.payload);
      expect(postParse).to.include(postPayload);

      const patchPayload = {
        phone: '412-444-5555',
      };
      const patchResponse = await server.inject({
        method: 'PATCH',
        url: `/landlords/${postParse.id}`,
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
        payload: patchPayload,
      });
      const patchParse = JSON.parse(patchResponse.payload);
      expect(patchParse).to.include({
        phone: "(412) 444-5555"
      });
    });

    lab.test('GET /accounts/landlords Contains One Landlord', async () => {
      const response = await server.inject({
        method: 'GET',
        url: '/accounts/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });
      const payload = JSON.parse(response.payload)
      expect(payload.length).to.equal(1);
    });

    // Add source and destination landlords to test merging
    lab.test('Setup landlords to merge', async () => { 
      const sourcePayload = {
        name: 'Source Landlord',
        body: 'Source Body',
        phone: '412-400-0000',
        website: 'https://source.com',
        // no email, destination should maintain own email
        AuthorId: accountId,
      };
      const sourceResponse = await server.inject({
        method: 'POST',
        url: '/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
        payload: sourcePayload,
      });
      const sourceParsePayload = JSON.parse(sourceResponse.payload);
      mergeSourceId = sourceParsePayload.id;
      mergeSourceName = sourceParsePayload.name;

      const destinationPayload = {
        name: 'Destination Landlord',
        body: 'Destination Body',
        phone: '412-500-0000', // both have phone, should move source phone into body
        //  no website, should take source
        email: 'destination@example.com',
        // no AuthorId
      };
      const destinationResponse = await server.inject({
        method: 'POST',
        url: '/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
        payload: destinationPayload,
      });
      
      const destinationParsePayload = JSON.parse(destinationResponse.payload);
      mergeDestinationId = destinationParsePayload.id;
    });    
    
    // Add Aliases to Source Landlord, to test if aliases transfer to destination
    lab.test('Setup aliases to merge', async () => {
      // wait for the response and the request to finish
      const aliasOnePayload = {
        aliasName: "Alias One"
      };
      const responseOne = await server.inject({
        method: 'POST',
        url: `/alias/landlord/${mergeSourceId}`,
        payload: aliasOnePayload,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
      const aliasTwoPayload = {
        aliasName: "Alias Two"
      };
      const responseTwo = await server.inject({
        method: 'POST',
        url: `/alias/landlord/${mergeSourceId}`,
        payload: aliasTwoPayload,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
    });

    //Post Review on source Landlord
    lab.test('Setup review to merge', async () => {
      // wait for the response and the request to finish
      const postPayload = {
        subject: 'Testing Landlord Review',
        body: 'This is a testing landlord review to merge.',
        rating: 5,
      };
      const response = await server.inject({
        method: 'POST',
        url: `/landlords/${mergeSourceId}/reviews`,
        payload: postPayload,
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });
    });
    //Post Property to merge
    lab.test('Setup property, for merge', async () => {
      // wait for the response and the request to finish
      const response = await server.inject({
        method: 'POST',
        url: '/properties',
        payload: {
          'address': '1640 Duffield St, Pgh',
        }
      });
      const payload = JSON.parse(response.payload);
      mergePropertyId = payload.id;
    });
    // Attach property to source landlord
    lab.test('Setup add landlord to property, for merge', async () => {
      const response = await server.inject({
        method: 'POST',
        url: `/properties/${mergePropertyId}/landlord`,
        payload: {
          id: `${mergeSourceId}`,
        }
      });

    });
    lab.test('PATCH /landlords/{id}/merge, Merge landlord info', async () =>{
      const mergePayload = {
        mergeInto: mergeDestinationId,
      };
    
      await server.inject({
        method: 'PATCH',
        url: `/landlords/${mergeSourceId}/merge`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
        payload: mergePayload,
      });

      const response = await server.inject({
        method: 'GET',
        url: `/landlords/${mergeDestinationId}`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });

      const payload = JSON.parse(response.payload);

      expect(payload).to.include({
        name: 'Destination Landlord',
        phone: '(412) 500-0000',
        website: 'https://source.com',
        email: 'destination@example.com',
        AuthorId: `${accountId}`,
        body: 
          'Destination Body\n' + 
          'Source Body\n' +
          'Alternate Info:\n' + 
          'Name: Source Landlord\n' + 
          'Body: Source Body\n' +
          'Phone: (412) 400-0000' 
      });
      expect(payload.metadata.propertyCount).to.equal(1);
    });

    // check if aliases exist on landlord
    lab.test('GET /alias/landlord/{landlordId}, alias merge', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/alias/landlord/${mergeDestinationId}`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
      const payload = JSON.parse(response.payload);
      expect(payload[0]).to.include({
        alias: mergeSourceName,
        aliasableType: 'landlord',
        aliasableId: mergeDestinationId,
      });
      expect(payload[1]).to.include({
        alias: 'Alias One',
      });
      expect(payload[2]).to.include({
        alias: 'Alias Two'
      });
    });

    // Get landlord by id with aliases
    lab.test('GET /landlords/{landlordId}, get LL with alias, id', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/landlords/${mergeDestinationId}`
      });

      const payload = JSON.parse(response.payload);
      expect(payload).to.include({
        name: 'Destination Landlord',
      });
      expect(payload.Aliases[0]).to.include({
        alias: 'Source Landlord'
      });
    });
    // Get landlord by machine name with aliases
    lab.test('GET /landlords/machine-name/{machine-name}, get LL with alias, machine-name', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/landlords/machine-name/destination-landlord`
      });

      const payload = JSON.parse(response.payload);
      expect(payload).to.include({
        name: 'Destination Landlord'
      });
      expect(payload.Aliases[0]).to.include({
        alias: 'Source Landlord'
      });
    });

      // Get reviews on destination Landlord
    lab.test('GET /landlords/{landlordId}/reviews, review merge', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/landlords/${mergeDestinationId}/reviews`
      });

      const payload = JSON.parse(response.payload);
      expect(payload[0]).to.include({
        subject: 'Testing Landlord Review',
        body: 'This is a testing landlord review to merge.',
        rating: 5,
      });
    });

    lab.test('GET /properties/{propertyId}, review merge', async () => {
      const response = await server.inject({
        method: 'GET',
        url: `/properties/${mergePropertyId}`,
      });

      const payload = JSON.parse(response.payload);
      expect(payload.LandlordId).to.equal(mergeDestinationId);
    });

    lab.test('Test Review Count', async () => {
      // Post a landlord to review
      const postLLPayload = {
        quickInfo: 'Reviewed Landlord',
      };
      const landlordResponse = await server.inject({
        method: 'POST',
        url: '/landlords',
        payload: postLLPayload,
      });
      const landlord = JSON.parse(landlordResponse.payload);

      // Add First Review
      const postFirstReviewPayload = {
        subject: 'Review 1',
        body: 'This is a review to test review count.',
        rating: 5,
      };
      await server.inject({
        method: 'POST',
        url: `/landlords/${landlord.id}/reviews`,
        payload: postFirstReviewPayload,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });

      // Check for 1 review 
      const response1 = await server.inject({
        method: 'GET',
        url: `/landlords/${landlord.id}`,
      });
      const payload1 = JSON.parse(response1.payload);
      expect(payload1.metadata).to.include({reviewCount: 1});

      // Add Second Review
      const postSecondReviewPayload = {
        subject: 'Review 2',
        body: 'This is another review to test review count.',
        rating: 5,
      };
      await server.inject({
        method: 'POST',
        url: `/landlords/${landlord.id}/reviews`,
        payload: postSecondReviewPayload,
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });

      // Check for 2 reviews
      const response2 = await server.inject({
        method: 'GET',
        url: `/landlords/${landlord.id}`,
      });
      const payload2 = JSON.parse(response2.payload);
      expect(payload2.metadata).to.include({reviewCount: 2});

      await server.inject({
        method: 'DELETE',
        url: '/landlords/' + landlord.id,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
    });

    // Attach property to new landlord
    lab.test('PATCH /properties/{id}/landlords, Move Property to new LL ', async () => {
      // Make a property
      let response = await server.inject({
        method: 'POST',
        url: '/properties',
        payload: {
          'address': '1724 Duffield St, Pgh',
        }
      });
      let property = JSON.parse(response.payload);

      // Make a landlord to attach property to
      response = await server.inject({
        method: 'POST',
        url: '/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
        payload: {
          'name': 'Landlord with Property',
        }
      });
      let formerLandlord = JSON.parse(response.payload);
      
      // Attach property to landlord
      await server.inject({
        method: 'POST',
        url: `/properties/${property.id}/landlord`,
        payload: {
          id: formerLandlord.id,
        }
        
      });
      
      // Get former landlord updated with attached Property
      response = await server.inject({
        method: 'GET',
        url: `/landlords/${formerLandlord.id}`,
      });

      formerLandlord = JSON.parse(response.payload);
      expect(formerLandlord.metadata.propertyCount).to.equal(1);

      // Make a second landlord to switch property to
      response = await server.inject({
        method: 'POST',
        url: '/landlords',
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
        payload: {
          'name': 'Landlord To Transfer Property',
        }
      });
      let newLandlord = JSON.parse(response.payload);      

      response = await server.inject({
        method: 'PATCH',
        url: `/properties/${property.id}/landlord`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
        payload: {
          id: newLandlord.id,
        }
      });

      let updatedProperty = JSON.parse(response.payload);

      // Get landlords updated after Property changed landlords
      response = await server.inject({
        method: 'GET',
        url: `/landlords/${formerLandlord.id}`,
      });

      formerLandlord = JSON.parse(response.payload);

      // Get new landlord updated with attached Property
      response = await server.inject({
        method: 'GET',
        url: `/landlords/${newLandlord.id}`,
      });

      newLandlord = JSON.parse(response.payload);

      expect(formerLandlord.metadata.propertyCount).to.equal(0);
      expect(newLandlord.metadata.propertyCount).to.equal(1);

      expect(updatedProperty.LandlordId).to.equal(newLandlord.id)

      // Remove property from landlord
      response = await server.inject({
        method: 'DELETE',
        url: `/properties/${property.id}/landlord`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });

      let propertySansLandlord = JSON.parse(response.payload);
      // Make sure landlord removed from property
      expect(propertySansLandlord.LandlordId).to.equal(null);

      // Get landlord updated detached from Property
      response = await server.inject({
        method: 'GET',
        url: `/landlords/${newLandlord.id}`,
      });

      newLandlord = JSON.parse(response.payload);
      //  Make sure landlord's propertyCount was decremented
      expect(newLandlord.metadata.propertyCount).to.equal(0);
      
      //  Clean Up
      await server.inject({
        method: 'DELETE',
        url: `/landlords/${formerLandlord.id}`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
      await server.inject({
        method: 'DELETE',
        url: `/landlords/${newLandlord.id}`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
      await server.inject({
        method: 'DELETE',
        url: `/properties/${property.id}`,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });
    });

    // DELETE from user account, fail
    lab.test('DELETEs /landlords/{landlordId}, user fail', async () => {

      // wait for the response and the request to finish
      const response1 = await server.inject({
        method: 'DELETE',
        url: '/landlords/' + landlordId,
        headers: {
          'Authorization': `bearer ${accessToken}`
        },
      });

      const payload1 = JSON.parse(response1.payload);
      // one row should have been affected
      expect(response1.statusCode).to.equal(403);
    });

    // DELETE from Admin account, success
    lab.test('DELETEs /landlords/{landlordId}, admin', async () => {

      // wait for the response and the request to finish
      const response1 = await server.inject({
        method: 'DELETE',
        url: '/landlords/' + landlordId,
        headers: {
          'Authorization': `bearer ${adminAccessToken}`
        },
      });

      const payload1 = JSON.parse(response1.payload);
      // one row should have been affected
      expect(payload1).to.equal(1);
      expect(response1.statusCode).to.equal(202);
    });
});

lab.experiment('Landlord - Locations', () => {
  let server;
  let accessToken;
  let adminAccessToken;

  let property1;
  let property2;
  let property3;
  let locationId;
  let landlordOneProp;
  let landlordTwoProp;

  lab.before(async() => {
    const index = await require('../../index.js');
    server = await index.server;

    // Login
    const tokenResponse = await getAccessToken();
    accessToken = tokenResponse.access_token;

    // Login
    const adminTokenResponse = await getAdminAccessToken();
    adminAccessToken = adminTokenResponse.access_token;

    // Create 3 properties in a location
    let response = await server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '4502 Butler St, Pittsburgh, PA 15201',
      }
    });
    property1 = JSON.parse(response.payload);

    response = await server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '4300 Butler St, Pittsburgh, PA 15201',
      }
    });
    property2 = JSON.parse(response.payload);

    response = await server.inject({
      method: 'POST',
      url: '/properties',
      payload: {
        'address': '4605 Butler St, Pittsburgh, PA 15201',
      }
    });
    property3 = JSON.parse(response.payload);
        
    // Store location of properties
    locationId = property1.LocationId;

    // Create 2 landlords
    response = await server.inject({
      method: 'POST',
      url: '/landlords',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
      payload: {
        'name': 'Landlord With One Property',
      }
    });
    landlordOneProp = JSON.parse(response.payload);

    response = await server.inject({
      method: 'POST',
      url: '/landlords',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
      payload: {
        'name': 'Landlord With Two Properties',
      }
    });
    landlordTwoProp = JSON.parse(response.payload);

    // Attach properties to each landlord, 1 with 1 prop, 1 with other 2 props
    await server.inject({
      method: 'POST',
      url: `/properties/${property1.id}/landlord`,
      payload: {
        id: landlordOneProp.id,
      }
    });

    await server.inject({
      method: 'POST',
      url: `/properties/${property2.id}/landlord`,
      payload: {
        id: landlordTwoProp.id,
      }
    });

    await server.inject({
      method: 'POST',
      url: `/properties/${property3.id}/landlord`,
      payload: {
        id: landlordTwoProp.id,
      }
    });

  });

  lab.test('GET /landlords/location{locationId, Get landlords in a location', async () => {
    // Get landlords by location
    let response = await server.inject({
      method: 'GET',
      url: `/landlords/location/${locationId}`,
    });

    const landlordsInLocation = JSON.parse(response.payload);

    // Expect it to contain 2 landlords
    expect(landlordsInLocation.length).to.equal(2);
  });
  
  lab.after(async() => {
    // Cleanup: Destroy all alandlords and properties
    await server.inject({
      method: 'DELETE',
      url: `/landlords/${landlordOneProp.id}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });
    await server.inject({
      method: 'DELETE',
      url: `/landlords/${landlordTwoProp.id}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });
    await server.inject({
      method: 'DELETE',
      url: `/properties/${property1.id}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });
    await server.inject({
      method: 'DELETE',
      url: `/properties/${property2.id}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });
    await server.inject({
      method: 'DELETE',
      url: `/properties/${property3.id}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });
  });
});

function getAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.TEST_USER,
        password: process.env.TEST_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}

// submit a landlord with other fields and it works

// submit a landlord with quick_info field and it works

// submit a landlord with a mix of fields and it works

function getAdminAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.SUPER_ADMIN_USER,
        password: process.env.SUPER_ADMIN_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}
