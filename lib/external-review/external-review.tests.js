const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const lab = exports.lab = Lab.script();

lab.experiment('External Review', () => {
  let server;
  let landlordId = '';
  let propertyId = '';
  let accountId = '';
  let landlordReview;
  let propertyReview;
  let accessToken = '';
  let adminAccessToken;

  lab.before(async() => {
    const index = await require('../../index.js');
    server = await index.server;
    await index.sequelize;

    // Create a Landlord
    landlordId = await createLandlord(server);

    // Create a Property
    propertyId = await createProperty(server);

    // Login
    const tokenResponse = await getAccessToken();
    accessToken = tokenResponse.access_token;

    // Post an Account
    // TODO: Revise this as not actually necessary if we've already gone through Account tests
    const accountResponse = await server.inject({
      method: 'POST',
      url: '/accounts',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });

    // Login Admin
    const adminTokenResponse = await getAdminAccessToken();
    adminAccessToken = adminTokenResponse.access_token;
  });

  // Create an External Review on that Landlord
  lab.test('POST /landlords/{landlordId}/external-reviews', async () => {
    // wait for the response and the request to finish
    const postPayload = {
      url: 'https://example.com',
      date: new Date,
    };
    let response = await server.inject({
      method: 'POST',
      url: `/landlords/${landlordId}/external-reviews`,
      payload: postPayload,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    let payload = JSON.parse(response.payload);
    expect(payload.reviewableId).to.equal(landlordId);
    landlordReview = payload;

    response = await server.inject({
      method: 'GET',
      url: `/landlords/${landlordId}`,
    });

    payload = JSON.parse(response.payload);
    expect(payload.metadata.externalReviewCount).to.equal(1);
  });

  // Get external reviews on that Landlord
  lab.test('GET /landlords/{landlordId}/external-reviews', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: `/landlords/${landlordId}/external-reviews`
    });

    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();

    expect(payload).to.include(landlordReview);
  });

  // Create an External Review on that Property
  lab.test('POST /properties/{propertyId}/external-reviews', async () => {
    // wait for the response and the request to finish
    const postPayload = {
      url: 'https://example.com/property',
      date: new Date,
    };
    let response = await server.inject({
      method: 'POST',
      url: `/properties/${propertyId}/external-reviews`,
      payload: postPayload,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    let payload = JSON.parse(response.payload);
    expect(payload.reviewableId).to.equal(propertyId);
    propertyReview = payload;

    response = await server.inject({
      method: 'GET',
      url: `/properties/${propertyId}`,
    });

    payload = JSON.parse(response.payload);
    expect(payload.metadata.externalReviewCount).to.equal(1);
  });

  // Get external reviews on that Property
  lab.test('GET /properties/{propertyId}/external-reviews', async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
      method: 'GET',
      url: `/properties/${propertyId}/external-reviews`
    });

    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();

    expect(payload).to.include(propertyReview);
  });
});

function getAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.TEST_USER,
        password: process.env.TEST_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}
  
function getAdminAccessToken() {
  
  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.SUPER_ADMIN_USER,
        password: process.env.SUPER_ADMIN_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}
  
async function createLandlord(server) {
  const postPayload = {
    quickInfo: 'Landlord for External Reviewable ',
  };
  const response = await server.inject({
    method: 'POST',
    url: '/landlords',
    payload: postPayload,
  });
  const payload = JSON.parse(response.payload);
  return payload.id;
}
  
async function createProperty(server) {
  const response = await server.inject({
    method: 'POST',
    url: '/properties',
    payload: {
      'address': '1701 Duffield St, Pgh',
    }
  });
  const payload = JSON.parse(response.payload);
  return payload.id;
}