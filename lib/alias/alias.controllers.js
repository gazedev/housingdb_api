module.exports = (models) => {
  const Boom = require('@hapi/boom');
  
  return {
    getLandlordAliases: async function (request, h){
      const landlordId = request.params.landlordId;
      return await getAliases(landlordId, 'landlord');
    },
    getPropertyAliases: async function (request, h){
      const propertyId = request.params.id;
      return await getAliases(propertyId, 'property');
    },
    createLandlordAlias: async function (request, h){
      const aliasName = request.payload.aliasName;
      const landlordId = request.params.landlordId;

      const alias = createLandlordAlias(aliasName, landlordId)
      return alias;
    },
    patchAlias: async function (request, h){
      const aliasId = request.params.aliasId;

      let aliasInstance;
      try {
        aliasInstance = await models.Alias.findOne({where: {id: aliasId}});
      } catch (e) {
        throw Boom.badImplementation('Error getting Alias', e);
      }

      let aliasObject = {};
      if(request.payload.alias) {
        aliasObject.alias = request.payload.alias;
      }
      if(request.payload.aliasableType) {
        aliasObject.aliasableType = request.payload.aliasableType;
      }
      if(request.payload.aliasableId) {
        aliasObject.aliasableId = request.payload.aliasableId;
      }
  
      let updatedAlias;
      try {
        updatedAlias = await aliasInstance.update(aliasObject);
      } catch (e){
        throw Boom.badImplementation('Error updating Alias. ' + e);
      }

      return updatedAlias;
    },
    deleteAlias: async function (request, h){
      let response;
      try {
        response = await models.Alias.destroy({
          where: {
            id: request.params.aliasId,
          }
        })
      } catch (e) {
        throw Boom.badImplementation('Error deleting landlord alias', e);
      }
      return h
        .response(response)
        .code(202);
    },
    lib: {
      createLandlordAlias: createLandlordAlias,
    }
  };

  function getAliases(aliasableId, aliasableType) {
    return models.Alias.findAll({
      where: {
        aliasableType: aliasableType,
        aliasableId: aliasableId,
      }
    });
  };

  async function createLandlordAlias (aliasName, landlordId, options = {} ){
    let aliasObject = {
      alias: aliasName,
      aliasableType: 'landlord',
      aliasableId: landlordId,
    };

    let alias;
    try {
      alias = await models.Alias.create(aliasObject, options);
    } catch (e){
      throw Boom.badImplementation('Error during createAlias(aliasObject). ' + e);
    }
    return alias;
  };
};
