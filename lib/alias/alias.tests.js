const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const lab = exports.lab = Lab.script();

lab.experiment('Alias', () => {
  let server;

  let accountId;
  let accessToken;
  let adminAccessToken;
  let landlordId;
  let landlordId2;
  let aliasId;


  lab.before(async() => {
    const index = await require('../../index.js');
    server = await index.server;
    let sequelize = await index.sequelize;

    // Login
    const tokenResponse = await getAccessToken();
    accessToken = tokenResponse.access_token;

    // Post an Account
    const accountResponse = await server.inject({
      method: 'GET',
      url: '/accounts',
      headers: {
        'Authorization': `bearer ${accessToken}`
      },
    });

    accountId = JSON.parse(accountResponse.payload).id;

    // Create a Landlord
    landlordId = await createLandlord(server);

    // Login
    const adminTokenResponse = await getAdminAccessToken();
    adminAccessToken = adminTokenResponse.access_token;
  });

  // Add an alias to Landlord
  lab.test('POST /alias/landlord/{landlordId}', async () => {
    const response = await server.inject({
      method: 'POST',
      url: `/alias/landlord/${landlordId}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
      payload: { aliasName: "Alternate Name" },
    });
    const payload = JSON.parse(response.payload)
    aliasId = payload.id;

    expect(payload).to.include({
      alias: 'Alternate Name',
      aliasableType: 'landlord', 
      aliasableId: landlordId
    });
  });

  //Search for landlord by Query
  lab.test('GET /Landlords, alias via search', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/landlords?search=Alternate',
    });
    const payload = JSON.parse(response.payload);
    expect(payload[0].id).to.equal(landlordId);
  })
  //Search for landlord by Query
  lab.test('GET /Landlords, alias via name', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/landlords?name=Alternate',
    });
    const payload = JSON.parse(response.payload);
    expect(payload[0].id).to.equal(landlordId);
  })
  // Attempt to add another landlord where alias name exists
  lab.test('POST /landlords, fail', async () => {
    const postPayload = {
      quickInfo: 'alternate name',
    };
    const response = await server.inject({
      method: 'POST',
      url: '/landlords',
      payload: postPayload,
    });
    const payload = JSON.parse(response.payload);
    expect(payload).to.include({statusCode: 422, error: 'Unprocessable Entity', message: 'Landlord with that name already exists'});
  })

  // Edit name of alias
  lab.test('PATCH /alias/{aliasId}, edit alias name', async () => {
    // Make a new landlord to reassign alias to it
    const landlordResponse = await server.inject({
      method: 'POST',
      url: '/landlords',
      payload: {quickInfo: 'New Landlord'}
    });
    const landlordPayload = JSON.parse(landlordResponse.payload);
    landlordId2 = landlordPayload.id;

    const patchPayload = {
      alias: 'Changed Name!',
    }
    
    const response = await server.inject({
      method: 'PATCH',
      url: `/alias/${aliasId}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
      payload: patchPayload,
    });
    
    const payload = JSON.parse(response.payload);
    expect(payload).to.include({
      alias: 'Changed Name!',
    });
  });

  // Edit landlordId of alias
  lab.test('PATCH /alias/{aliasId}, reassign alias', async () => {
    const patchPayload = {
      aliasableId: landlordId2,
    }
    
    const response = await server.inject({
      method: 'PATCH',
      url: `/alias/${aliasId}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
      payload: patchPayload,
    });
    
    const payload = JSON.parse(response.payload);
    expect(payload).to.include({
      aliasableId: landlordId2,
    });
  });

  // Change aliasableType to invalid
  lab.test('PATCH /alias/{aliasId}, fail invalid aliasableType', async () => {
    const patchPayload = {
      aliasableType: 'lamblord',
    }
    
    const response = await server.inject({
      method: 'PATCH',
      url: `/alias/${aliasId}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
      payload: patchPayload,
    });
    
    const payload = JSON.parse(response.payload);
    expect(payload).to.include({
      statusCode: 400,
    });
  });

  // Delete an alias
  lab.test('DELETE /alias/{aliasId}, delete alias', async () => {
    const response = await server.inject({
      method: 'DELETE',
      url: `/alias/${aliasId}`,
      headers: {
        'Authorization': `bearer ${adminAccessToken}`
      },
    });

    const payload = JSON.parse(response.payload);
    expect(payload).to.equal(1);
  });
});

function getAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.TEST_USER,
        password: process.env.TEST_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}
function getAdminAccessToken() {

  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    const endpoint = new URL(process.env.JWT_NETWORK_URI + '/protocol/openid-connect/token');

    const options = {
      "method": "POST",
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    };

    var req = http.request(endpoint, options, function (res) {
      let chunks = "";

      res.on("data", function (chunk) {
        chunks += chunk;
      });

      res.on("end", function () {
        resolve(JSON.parse(chunks));
      });
    });

    req.write(
      qs.stringify({
        grant_type: 'password',
        username: process.env.SUPER_ADMIN_USER,
        password: process.env.SUPER_ADMIN_PASSWORD,
        client_id: process.env.JWT_CLIENT,
      })
    );
    req.end();

  });
}

async function createLandlord(server) {
  const postPayload = {
    quickInfo: 'Aliasable Landlord',
  };
  const response = await server.inject({
    method: 'POST',
    url: '/landlords',
    payload: postPayload,
  });
  const payload = JSON.parse(response.payload);
  return payload.id;
}