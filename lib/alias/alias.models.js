const Joi = require('joi');

module.exports = {
  db: (sequelize, Sequelize) => {
    const Alias = sequelize.define('Alias', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
      },
      alias: Sequelize.STRING,
      aliasableType: Sequelize.STRING, // 'landlord'/'property'
      aliasableId: Sequelize.UUID,
    });

    Alias.associate = function (models) {
      models.Landlord.hasMany(models.Alias, {
        foreignKey: 'aliasableId',
        constraints: false,
        scope: {
          aliasableType: 'landlord'
        }
      });
      models.Alias.belongsTo(models.Landlord, {
        foreignKey: 'aliasableId',
        constraints: false,
        as: 'landlord',
      });
      models.Property.hasMany(models.Alias, {
        foreignKey: 'aliasableId',
        constraints: false,
        scope: {
          aliasableType: 'property'
        }
      });
      models.Alias.belongsTo(models.Property, {
        foreignKey: 'aliasableId',
        constraints: false,
        as: 'property',
      });
    };
    
    return Alias;
  },
  landlordId: Joi.object().keys({
    landlordId: Joi.string().guid().required(),
  }),
  alias: Joi.object().keys({
    aliasName: Joi.string(),
  }),
  aliasId: Joi.object().keys({
    aliasId: Joi.string().guid().required(),
  }),
  aliasPatch: Joi.object().keys({
    alias: Joi.string(),
    aliasableType: Joi.string().valid('landlord', 'property'),
    aliasableId: Joi.string().guid(),
  }),
};
