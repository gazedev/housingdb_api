module.exports = {
  routes: (models) => {
    const helpers = require('../helpers')(models);
    const controllers = require('./alias.controllers')(models);
    const aliasModels = require('./alias.models');
    const landlordModels = require('../landlord/landlord.models');
    
    return [
      {
        method: 'GET',
        path: '/alias/landlord/{landlordId}',
        handler: controllers.getLandlordAliases,
        options: {
          description: 'Get Landlord Aliases',
          notes: 'Returns all aliases for a landlord.',
          tags: ['api', 'Aliases'],
          validate: {
            params: aliasModels.landlordId,
          }
        }
      },
      {
        method: 'POST',
        path: '/alias/landlord/{landlordId}',
        handler: controllers.createLandlordAlias,
        options: {
          pre: [
            { method: helpers.ensureAdmin, failAction: 'error' },
          ],
          description: 'Create Landlord Aliases',
          notes: 'Creates a new alias for a landlord.',
          tags: ['api', 'Aliases'],
          validate: {
            params: aliasModels.landlordId,
            payload: aliasModels.alias,
          }
        }
      },
      {
        method: 'PATCH',
        path: '/alias/{aliasId}',
        handler: controllers.patchAlias,
        options: {
          pre: [
            { method: helpers.ensureAdmin, failAction: 'error' },
          ],
          description: 'Update Landlord Alias',
          notes: 'Updates an alias for a landlord.',
          tags: ['api', 'Aliases'],
          validate: {
            params: aliasModels.aliasId,
            payload: aliasModels.aliasPatch,
          }
        }
      },
      {
        method: 'DELETE',
        path: '/alias/{aliasId}',
        handler: controllers.deleteAlias,
        options: {
          pre: [
            { method: helpers.ensureAdmin, failAction: 'error' },
          ],
          description: 'Delete Aliases',
          notes: 'Delete an alias by id.',
          tags: ['api', 'Aliases'],
          validate: {
            params: aliasModels.aliasId,
          }
        }
      },
    ];
  },
};
