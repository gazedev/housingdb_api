module.exports = {
  routes: (models) => {
    const controllers = require('./eviction.controllers')(models);
    const landlordModels = require('../landlord/landlord.models');

    return [
      {
        method: 'GET',
        path: '/landlords/machine-name/{machineName}/evictions',
        handler: controllers.getLandlordEvictionsByMachineName,
        options: {
          auth: false,
          description: 'Get the Evictions associated with a Landlord',
          notes: 'This endpoint currently looks up evictions based on the landlord name and landlord aliases from the evictions api',
          tags: ['api', 'Evictions'],
          validate: {
            params: landlordModels.machineName,
          }
        }
      },
    ];

  },
};