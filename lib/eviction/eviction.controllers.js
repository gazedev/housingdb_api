module.exports = (models) => {
  const Boom = require('@hapi/boom');
  const landlordLib = require('../landlord/landlord.controllers')(models).lib;
  return {
    lib: {
      capabilityEvictionsSystem
    },
    getLandlordEvictionsByMachineName: async function(request, h) {
      // make the call to the capabilityEvictionsSystem to see if this endpoint should be accessible
      if (!capabilityEvictionsSystem()) {
        throw Boom.notImplemented();
      }
      
      let returnLandlord;
      try {
        returnLandlord = await landlordLib.getLandlordByMachineName(request.params.machineName);
      } catch (e) {
        throw Boom.badImplementation('Error getting landlord' + e);    
      };
      if(returnLandlord === null) {
        return Boom.notFound('Cannot find this landlord'); 
      };
      const landlordName = returnLandlord.name;
      const landlordAliases = returnLandlord.Aliases.map((item) => {
        return item.alias
      });
      const possibleNames = [landlordName, ...landlordAliases].map((name) => {
        return '%' + name + '%';
      });

      const response = await getEvictionsByFuzzyPlaintiffs(possibleNames);
      return response;
    }
  };

  function capabilityEvictionsSystem () {
    const evictionApi = process.env.EVICTION_API;
    return !!evictionApi;
  }

  function getEvictionsByFuzzyPlaintiffs(plaintiffs) {
    const api = new URL(`${process.env.EVICTION_API}/evictions`);

    let https;
    if (api.protocol == 'http:') {
      https = require('http');
    } else {
      https = require('https');
    }
    
    if (process.env.EVICTION_API_COUNTY_LIMIT) {
      api.searchParams.set('county', process.env.EVICTION_API_COUNTY_LIMIT);
    }

    for (let plaintiff of plaintiffs) {
      api.searchParams.append('plaintiff', `%${plaintiff}%`);
    }

    console.log("making evictions api request", api);

    let promise = new Promise((resolve, reject) => {
      https.get(api, res => {
        res.setEncoding("utf8");
        let body = "";
        res.on("data", data => {
          body += data;
        });
        res.on("end", () => {
          try {
            body = JSON.parse(body);
          } catch (e) {
            // log the error, but don't throw here or we will crash the api
            console.log(e);         
          }
          resolve(body);
        });
        res.on('error', error => {
          reject(error);
        });
      });

    });

    return promise;
  };
  
}