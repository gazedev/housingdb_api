#!/bin/bash
# This script makes assumptions about where you are in the filesystem
# so this makes sure it executes from the context it thinks it is
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

git pull

docker-compose --file=docker-compose-prod.yml stop

docker-compose --file=docker-compose-prod.yml up -d
