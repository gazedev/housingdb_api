'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('Aliases', 'createdAt', {
      type: Sequelize.DATE,
    });
    await queryInterface.addColumn('Aliases', 'updatedAt', {
      type: Sequelize.DATE,
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     return queryInterface.removeColumn('Aliases', 'createdAt');
     return queryInterface.removeColumn('Aliases', 'updatedAt');
  }
};
