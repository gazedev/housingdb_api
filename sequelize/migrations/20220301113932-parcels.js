'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const t = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('Parcels', {
        uuid: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        parcelId:  Sequelize.STRING,
        PropertyId: {
          type: Sequelize.UUID,
          references: {
            model: 'Properties', // name of Target model
            key: 'id', // key in Target model that we're referencing
          },
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      }, { transaction: t });
      
      await t.commit();
    } catch (err) {
      await t.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.dropTable('Parcels');
  }
};
