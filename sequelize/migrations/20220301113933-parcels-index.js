'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const t = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addIndex('Parcels', ['parcelId'], {
        unique: false,
        transaction: t
      });
      
      await t.commit();
    } catch (err) {
      await t.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
  }
};
