#!/bin/bash
source variables-prod.env

echo "NGINX_NAMESPACE is ${NGINX_NAMESPACE}"

# Make a copy of our nginx file so we can rewrite placeholders
cp nginx.conf ${NGINX_NAMESPACE}.conf

sed -i -e "s|NGINX_SERVER_NAME|${NGINX_SERVER_NAME}|" ${NGINX_NAMESPACE}.conf
sed -i -e "s|NGINX_PROXY_PASS_PORT|${NGINX_PROXY_PASS_PORT}|" ${NGINX_NAMESPACE}.conf

# Copy our config to the nginx sites-available directory, with a more specific name
sudo mv ${NGINX_NAMESPACE}.conf /etc/nginx/sites-available/${NGINX_NAMESPACE}

# Symlink nginx config to sites-enabled to enable it
sudo ln -sf /etc/nginx/sites-available/${NGINX_NAMESPACE} /etc/nginx/sites-enabled

# To check for typos in your file:
sudo nginx -t

# If you get no errors, you can restart nginx:
sudo service nginx restart

# Let's Encrypt
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update
sudo apt install python-certbot-nginx -y

sudo certbot \
  --noninteractive \
  --nginx \
  -d ${NGINX_SERVER_NAME} \
  -m ${CERTBOT_EMAIL_ADDRESS} \
  --agree-tos \
  --no-eff-email \
  --redirect

# To check for typos in your file:
sudo nginx -t

# If you get no errors, you can restart nginx to apply the changes:
sudo service nginx restart
